﻿namespace Question8.Domain
{
    public class RuleForThings
    {
        public string Thing { get; }
        public string Behavior { get; }

        public RuleForThings(string thing, string behavior)
        {
            Thing = thing;
            Behavior = behavior;
        }
    }
}
